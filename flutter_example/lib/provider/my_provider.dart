import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_example/model/user_model.dart';
import 'package:flutter_example/service/api_service.dart';

class MyProvider with ChangeNotifier, DiagnosticableTreeMixin {
  List<UserModel> _userInfo = [];
  List<UserModel> get userInfo => _userInfo;

  Future<void> getUser() async {
    var res = await APIService().getUser();
    _userInfo = userModelFromJson(res);
    notifyListeners();
  }
}
