import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_example/provider/my_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';

class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  late TextEditingController _emailController;

  String _emailTextValue = "";

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 70),
        alignment: Alignment.topCenter,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Second Screen",
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            _userNameTextfieldWidget(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlinedButton(
                  onPressed: () {
                    setState(() {
                      _emailTextValue = _emailController.value.text;
                    });
                  },
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                  child: const Text("Get email"),
                ),
                SizedBox(
                  width: 10,
                ),
                OutlinedButton(
                  onPressed: () {
                    setState(() {
                      _emailTextValue = "";
                      _emailController.clear();
                    });
                  },
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                  child: const Text("Clear email"),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("My email : "),
                Text(_emailTextValue),
              ],
            ),
            Divider(
              height: 20,
            ),
            OutlinedButton(
              onPressed: () async {
                final pref = await SharedPreferences.getInstance();
                await pref.setString("my_email", _emailTextValue);
                showSanckBar("success");
              },
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
              ),
              child: const Text("Save email to sharepreference"),
            ),
            OutlinedButton(
              onPressed: () async {
                final pref = await SharedPreferences.getInstance();
                var myEmail = await pref.getString("my_email");

                showSanckBar(myEmail!);
              },
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
              ),
              child: const Text("Show email from sharepreference"),
            ),
            Divider(
              height: 20,
            ),
            OutlinedButton(
              onPressed: () async {
                await context.read<MyProvider>().getUser();
              },
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
              ),
              child: const Text("Get data from server"),
            ),
            Expanded(
              child: Consumer(builder:
                  (BuildContext context, MyProvider myProvider, Widget? child) {
                return ListView.builder(
                    itemCount: myProvider.userInfo.length,
                    itemBuilder: (context, index) {
                      return Container(
                        width: double.infinity,
                        height: 100,
                        margin: EdgeInsets.only(left: 20,right: 20,bottom: 5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 0,
                                blurRadius: 2,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],color: Colors.white),
                        child: ListTile(
                          title: Text(myProvider.userInfo[index].username!),
                          subtitle: Text(myProvider.userInfo[index].email!),
                        ),
                      );
                    });
              }),
            )
          ],
        ),
      ),
    );
  }

  _userNameTextfieldWidget() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Theme(
        data: Theme.of(context).copyWith(
            hintColor: Color(0x1AF56827),
            primaryColor: Colors.deepOrangeAccent),
        child: TextFormField(
          autovalidate: true,
          controller: _emailController,
          validator: (value) => EmailValidator.validate(value!)
              ? null
              : "Please enter a valid email",
          style: TextStyle(color: Colors.black, fontSize: 14),
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.only(top: 7),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(90.0)),
                  borderSide: BorderSide(
                    color: Colors.deepOrangeAccent,
                  )),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(90.0)),
                  borderSide: BorderSide(
                    color: Colors.transparent,
                  )),
              prefixIcon: Padding(
                padding: const EdgeInsets.all(14.0),
                child: Icon(
                  Icons.alternate_email,
                  color: Colors.deepOrangeAccent,
                ),
              ),
              hintStyle: TextStyle(
                color: Colors.black54,
              ),
              labelStyle: TextStyle(
                color: Colors.black54,
              ),
              filled: true,
              fillColor: Color(0x8F56827),
              hintText: 'Email',
              labelText: "Email"),
          onChanged: (value) {},
        ),
      ),
    );
  }

  showSanckBar(String textAlert) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          textAlert,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
