import 'package:http/http.dart' as http;

class APIService {
  Future<String> getUser() async {
    var url = Uri.https('jsonplaceholder.typicode.com', '/users');
    var response = await http.get(url);
    if (response.statusCode == 200 || response.statusCode == 400) {
      var res = response.body;
      return res;
    } else {
      throw Exception('Failed To Get Form Server');
    }
  }
}
